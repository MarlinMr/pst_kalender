a='abcdefghijklmnopqrstuvwxyz'
b='abcdefghijklmnopqrstuvwxyzæøå'
c='abcdefghijklmnopqrstuvwxyzæøå0123456789'

ciphers=['KNO', 'fmwggkymyioån', '30å6ø8432æå54710a9æ09a305å7z9829', 'fmwggkymyioån', 'ngpoo']

betlist=[a,b,c,b,a]
cipher = ciphers[0]
keylist=[]
i=0
for cipher in ciphers:
    bet = betlist[i]
    for key in range(len(bet)):
        pt=""
        for symbol in cipher:
            num = bet.find(symbol.lower())
            pt=str(pt)+bet[(num+key)%len(bet)]
        print(f"Nøkkel {key:>2} gir : {pt}")
    key=input('Hvilken nøkkel skal brukes : ')
    keylist.append(key)
    i+=1
    
i=0
plaintext=""
for cipher in ciphers:
    bet = betlist[i]
    pt=""
    for symbol in cipher:
        num = bet.find(symbol.lower())
        pt=str(pt)+bet[(num+int(keylist[i]))%len(bet)]
    plaintext = plaintext + " " + pt
    i+=1
print(plaintext)
